<?php
require "begin.html";
require 'cnx.php';

// $requet = ' SELECT * FROM nobels ORDER BY year DESC LIMIT 25';
// $ex = $cnx->query($requet);

require_once "Model.php";
$e = new Model;
$ex = $e->get_last(); ?>
<h1>List of the last 25 Nobel prizes</h1>

<table>
    <tr>

        <th>Name</th>
        <th>Category</th>
        <th>Year</th>
        <th colspan="2">action</th>

    </tr>
    <?php

    function sp($s)
    {
        return htmlspecialchars($s, ENT_QUOTES);
    }

    foreach ($ex as $x) { ?>

        <tr>
            <?php echo '<td> <a href="informations.php?id=' . sp($x['id']) . '">' . $x[3] . "</a></td>" ?>
            <td><?php echo $x[2] ?> </td>
            <td><?php echo $x[1] ?> </td>
            <?php echo '<td><a href="remove.php?id=' . sp($x["id"]) . '"><img src="Content/img/remove-icon.png" alt="delete"/></a></td>' ?>
            <?php echo '<td><a href="update.php?id=' . sp($x["id"]) . '"><img src="Content/img/edit-icon.png" alt="update"/></a></td>' ?>
        </tr>
    <?php
    }
    ?>
</table>
<?php
?>